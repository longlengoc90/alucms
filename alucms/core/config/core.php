<?php
/**
 * Author: Le Ngoc Long
 * Email: longlengoc90@gmail.com
 * Phone: 078.223.6969
 */

return [
    'admin_route' => env('SOURCE_ADMIN_ROUTE', 'admincp'),
];
