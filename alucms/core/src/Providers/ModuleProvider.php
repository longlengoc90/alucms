<?php
/**
 * Author: Le Ngoc Long
 * Email: longlengoc90@gmail.com
 * Phone: 078.223.6969
 * Class ModuleProvider
 * @package AluCMS\Core\Providers
 */

namespace AluCMS\Core\Providers;

use AluCMS\Core\Supports\Helper;
use Illuminate\Support\ServiceProvider;

class ModuleProvider extends ServiceProvider
{
    public function boot()
    {

    }

    public function register()
    {
        //Load helpers
        Helper::loadModuleHelpers(__DIR__);

        /**
         * Config
         */
        $this->mergeConfigFrom(__DIR__ . '/../../config/core.php', 'core');

        /**
         * Seft providers
         */
        $this->app->register(RouteProvider::class);

        /**
         * Other module providers
         */
        $this->app->register(\AluCMS\Auth\Providers\ModuleProvider::class);
        $this->app->register(\AluCMS\User\Providers\ModuleProvider::class);
        $this->app->register(\AluCMS\Acl\Providers\ModuleProvider::class);
    }
}
